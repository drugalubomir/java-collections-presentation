# Java Collections Presentation

This repository includes:

- html presentation -> available also on [this page](https://drugalubomir.gitlab.io/java-collections-presentation/) 

- java code examples are located in [CollectionsExamples](CollectionsExamples.java) source code

- java collections exercises are located in [CollectionsExercises](CollectionsExercises.java) source code

- java collections exercises solution are located in [CollectionsExercisesSolution](CollectionsExercisesSolution.java) source code