
import java.util.*;
import java.util.stream.Collectors;

public class CollectionsExercisesSolution {

    public static void main(String[] args) {

        Collection<Integer> task1Integers = task1();
        System.out.format("Task 1: \n - size: %d \n - elements: %s\n", task1Integers.size(), task1Integers);

        List<Integer> task2Integers = task2();
        System.out.format("Task 2: \n - size: %d \n - elements: %s\n", task2Integers.size(), task2Integers);

        System.out.println(task3());
        System.out.println(task4());
    }


    /**
     * TASK 1
     * Je dany List -> integerList
     * - Ku kazdemu cislu v list-e priratajte 1
     * - cisla prefiltrujte, zachovajte iba parne a unikatne cisla
     *
     * @return vratte novu kolekciu (List, Set, ...), ktora bude obsahovat IBA unikatne parne cisla
     */
    public static Collection<Integer> task1() {

        List<Integer> integerList = Arrays.asList(77, 21, 95, 29, 57, 68, 40, 18, 31, 56, 20, 39, 74, 1, 28, 42, 20, 86, 2, 66,
                68, 51, 41, 53, 45, 36, 52, 90, 39, 100, 59, 54, 36, 29, 66, 43, 98, 54, 55, 57, 80, 50, 55, 79, 38, 48, 95, 63, 34, 32,
                62, 19, 31, 43, 33, 21, 51, 26, 9, 92, 35, 51, 36, 90, 3, 81, 4, 71, 62, 37, 93, 28, 56, 35, 36, 12, 65, 57, 52, 68, 80, 3,
                79, 61, 11, 73, 53, 73, 47, 35, 19, 20, 4, 30, 45, 100, 16, 72, 16, 46);

        return integerList
                .stream()
                .map(number -> number + 1)
                .filter(number -> number % 2 == 0)
                .collect(Collectors.toSet());
    }


    /**
     * TASK 2
     * Je dany List -> integerList
     * - Vasou ulohou je zoradit tieto cisla od najvacsieho po najmensie
     *
     * @return vratte novu kolekciu - List, ktora bude obsahovat zoradene cisla
     */
    public static List<Integer> task2() {

        List<Integer> integerList = Arrays.asList(77, 21, 95, 29, 57, 68, 40, 18, 31, 56, 20, 39, 74, 1, 28, 42, 20, 86, 2, 66,
                68, 51, 41, 53, 45, 36, 52, 90, 39, 100, 59, 54, 36, 29, 66, 43, 98, 54, 55, 57, 80, 50, 55, 79, 38, 48, 95, 63, 34, 32,
                62, 19, 31, 43, 33, 21, 51, 26, 9, 92, 35, 51, 36, 90, 3, 81, 4, 71, 62, 37, 93, 28, 56, 35, 36, 12, 65, 57, 52, 68, 80, 3,
                79, 61, 11, 73, 53, 73, 47, 35, 19, 20, 4, 30, 45, 100, 16, 72, 16, 46);

        List<Integer> sortedIntegers = integerList
                .stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());

        return sortedIntegers;
    }


    /**
     * TASK 3 - Desifrovanie
     * Je dany List -> lettersList
     * - Vasou ulohou je desifrovat odkaz ukryty pod cislami v lettersList
     * <p>
     * (pomocka - ASCII)
     *
     * @return desifrovany odkaz
     */
    public static String task3() {

        List<Integer> lettersList = Arrays.asList(65, 107, 32, 116, 111, 116, 111, 32, 99, 105, 116, 97, 116, 101,
                44, 32, 112, 111, 100, 97, 114, 105, 108, 111, 32, 115, 97, 32, 118, 97, 109, 32, 117, 115,
                112, 101, 115, 110, 101, 32, 114, 111, 122, 108, 117, 115, 116, 105, 116, 32, 116, 117, 116, 111,
                32, 117, 108, 111, 104, 117, 46, 32, 58, 41);

        return lettersList
                .stream()
                .map(asciiNumber -> (char) asciiNumber.intValue())
                .map(character -> character.toString())
//                .map(Object::toString) //method reference - alternative to map in line above
                .collect(Collectors.joining(""));
    }


    /**
     * TASK 4 - Desifrovanie 2
     * Je dany List -> letters
     * - Vasou ulohou je desifrovat odkaz ukryty pod pismenami v letters liste
     * <p>
     * - kazde pismeno je posunute o urcitu hodnotu podla ascii tabulky
     * <p>
     * - kazde pismeno v list-e napr. "a" je potrebne premenit na ASCII ciselnu hodnotu
     * - nasledne podla hodnoty (ci je parna alebo neparna) je potrebne vykonat +/- operaciu podla:
     * - ak je hodnota PARNA, priratame 2
     * - ak je hodnota NEPARNA, odratame 2
     * (pomocka - ASCII, char)
     *
     * @return desifrovany odkaz
     */
    public static String task4() {

        List<String> letters = Arrays.asList("C", "m", "a", "u", "r", "g", "a", "u", "e", "f", "q", "n", "l", "k",
                "a", "n", "p", "g", "e", "k", "r", "c", "r", "a", "c", "h", "a", "r", "q", "r", "q", "*", "a", "n",
                "q", "b", "c", "p", "k", "j", "q", "a", "u", "c", "a", "t", "c", "o", "a", "w", "u", "n", "g", "u",
                "l", "g", "a", "p", "q", "x", "j", "w", "u", "r", "k", "r", "a", "n", "q", "u", "j", "g", "b", "l",
                "w", "a", "x", "j", "q", "x", "k", "r", "g", "h", "u", "k", "w", "a", "u", "k", "d", "p", "w", ",",
                "a", "8", "+", "a", "X", "c", "a", "r", "w", "r", "q", "a", "w", "j", "q", "f", "w", "a", "b", "q",
                "u", "r", "c", "t", "c", "r", "g", "a", "h", "g", "b", "l", "q", "r", "m", "{", "a", "x", "c", "a",
                "c", "m", "r", "k", "t", "k", "r", "w", "8", "+");


//        //Solution 1
//        StringBuilder sb = new StringBuilder();
//
//        letters.forEach(letter -> {
//            char charLetter = letter.charAt(0);
//            int intValue = (int) charLetter;
//
//            if (intValue % 2 == 0) {
//                intValue += 2;
//            } else {
//                intValue -= 2;
//            }
//            char decodedCharacter = (char) intValue;
//            sb.append(decodedCharacter);
//        });
//
//        return sb.toString();

        //Solution 2
        return letters
                .stream()
                .map(letter -> letter.charAt(0))
                .map(charLetter -> {
                    int intValue = (int) charLetter;

                    if (intValue % 2 == 0) {
                        intValue += 2;
                    } else {
                        intValue -= 2;
                    }

                    return (char) intValue;
                })
                .map(character -> character.toString())
//                .map(Object::toString) //method reference - alternative to map in line above
                .collect(Collectors.joining(""));
    }

}