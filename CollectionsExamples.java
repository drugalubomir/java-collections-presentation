
import java.util.*;
import java.util.stream.Collectors;

public class CollectionsExamples {

    public static void main(String[] args) {

        List<Integer> nums = Arrays.asList(10, 29, 32, 14, 5, 7, 8, 23, 657, 32, 999);

        List<Integer> countedNums = new ArrayList<>();

        //version #1
        for (int i = 0; i < nums.size(); i++) {
            int number = nums.get(i);
            number = count(number);
            countedNums.add(number);
        }
        System.out.println("countedNums = " + countedNums);


        //version #2
        countedNums.clear();
        for (Integer number : nums) {
            countedNums.add(count(number));
        }
        System.out.println("countedNums = " + countedNums);


        //version #3
        countedNums.clear();
        nums.forEach(number -> {
            countedNums.add(count(number));
        });
        System.out.println("countedNums = " + countedNums);


        //version #4
        nums.forEach(number -> countedNums.add(count(number)));
        System.out.println("countedNums = " + countedNums);


        //STREAM METHODS EXAMPLES

        //.filter example
        List<Integer> numbersOver30 = nums.stream()
                .filter(number -> number > 30)
                .collect(Collectors.toList());

        System.out.printf("numbersOver30 = %s%n", numbersOver30);

        //.map example
        List<Integer> added10ToNumbers = nums.stream()
                .map(number -> number + 10)
                .collect(Collectors.toList());

        System.out.printf("added10ToNumbers = %s%n", added10ToNumbers);


        //.anyMatch example
        //has at least one number equal to 8
        boolean contains8 = nums.stream().anyMatch(number -> number == 8);
        System.out.printf("has at least one number equal to 8 = %b%n", contains8);


        //.noneMatch example
        //has not any number equal to 8
        boolean notContains8 = nums.stream().noneMatch(number -> number == 8);
        System.out.printf("does nums list NOT contains 8 = %b%n", notContains8);


        //.allMatch example
        //has ALL of the numbers equal to 8
        boolean allElements8 = nums.stream().allMatch(number -> number == 8);
        System.out.printf("has ALL of the numbers equal to 8 = %b%n", allElements8);


        List<Integer> allEights = Arrays.asList(8, 8, 8, 8, 8, 8);

        //.allMatch example
        //has ALL of the numbers equal to 8
        boolean allElements8_ = allEights.stream().allMatch(number -> number == 8);
        System.out.printf("has ALL of the numbers equal to 8 = %b%n", allElements8_);


        //.sum example
        //sum all numbers in a list
        int sumTotal = nums.stream().mapToInt(number -> number).sum();
        System.out.println("sumTotal = " + sumTotal);


        //.count example
        long countOfNumsOver30 = nums
                .stream()
                .filter(number -> number > 30)
                .count();
        System.out.println("countOfNumsOver30 = " + countOfNumsOver30);


        //.collect example
        List<String> lettersList = Arrays.asList("z", "a", "B", "b", "C", "c", "x", "A", "D", "e", "e", "K");

        Set<String> uniqueUppercaseLetters = lettersList
                .stream()
//                .map(letter -> letter.toUpperCase())  // alternative
                .map(String::toUpperCase)   // method reference
                .collect(Collectors.toSet());
        //todo ukazat collectors joining
        System.out.println("uniqueUppercaseLetters = " + uniqueUppercaseLetters);


        //Cash desk example
        Map<String, Double> codePriceMap = new HashMap<>();
        codePriceMap.put("100", 2.99);
        codePriceMap.put("101", 0.82);
        codePriceMap.put("102", 28.91);
        codePriceMap.put("103", 10.20);
        codePriceMap.put("104", 0.43);
        codePriceMap.put("105", 11.00);

        Map<String, Integer> codeCountMap = new HashMap<>();
        codeCountMap.put("100", 5);
        codeCountMap.put("101", 9);
        codeCountMap.put("102", 1);
        codeCountMap.put("103", 0);
        codeCountMap.put("104", 15);
        codeCountMap.put("105", 2);


        //counting total price using loop
        double totalPrice_ = 0d;
        for (Map.Entry<String, Integer> entry : codeCountMap.entrySet()) {
            int count = entry.getValue();
            double price = codePriceMap.get(entry.getKey());

            totalPrice_ += count * price;
        }
        System.out.println("totalPrice_ = " + totalPrice_);


        //counting total price using Stream API
        double totalPrice = codeCountMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 0)
                .mapToDouble((entry) -> codePriceMap.get(entry.getKey()) * entry.getValue())
                .sum();

        System.out.println("totalPrice = " + totalPrice);

    }

    public static int count(int number) {
        return (int) ((number + 2 * 5 / 2 + 33) * Math.PI);
    }

}
